package ua.com.futureprocessing;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import ua.com.futureprocessing.Pages.AboutUsPage;
import ua.com.futureprocessing.Pages.HomePage;
import ua.com.futureprocessing.Pages.SocialNetworksPage;


@RunWith(Parameterized.class)
public class SeleniumTest {

    public static WebDriver driver;
    public String browser;
    public static HomePage homePage;
    public static SocialNetworksPage socialNetworksPage;
    public static AboutUsPage aboutUsPage;

    @Parameters
    public static Collection getBrowser(){
        return Arrays.asList(new Object[][] {{"FF"},{"Chrome"},{"E"}});
    }
    public SeleniumTest(String browser){
        this.browser= browser;
    }
    @Before
    public void setup(){
        System.out.println("Browser:"+ browser);
        if(browser.equalsIgnoreCase("FF")){
            System.setProperty("webdriver.gecko.driver","E:/Home/testing/geckodriver-v0.19.1-win64/geckodriver.exe");
            driver = new FirefoxDriver();
        } else if(browser.equalsIgnoreCase("E")){
            System.setProperty("webdriver.edge.driver","E:/Home/testing/edgedriver/MicrosoftWebDriver.exe");
            driver = new EdgeDriver();
        } else if(browser.equalsIgnoreCase("Chrome")){
            System.setProperty("webdriver.chrome.driver", "E:/Home/testing/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
        homePage = new HomePage(driver);
        }
        startPosition();
    }

    public static void startPosition() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.future-processing.com/");
    }
    @Test
    public void subMenuTest()
    {
        try {
            homePage.mouseOverAboutUs();
            String aboutUsHeader = homePage.getHeaderAboutUs();
            Assert.assertEquals("ABOUT US", aboutUsHeader);
            homePage.mouseOverServices();
            String headerServices = homePage.getHeaderServices();
            Assert.assertEquals("SERVICES", headerServices);
            homePage.mouseOverSolutions();
            String headerSolutions = homePage.getHeaderSolutions();
            Assert.assertEquals("SOLUTIONS", headerSolutions);
            homePage.mouseOverExpertise();
            String headerExpertise = homePage.getHeaderExpertise();
            Assert.assertEquals("TECHNOLOGIES", headerExpertise);
            homePage.mouseOverClients();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void listOfImage(){
        try {
        homePage.clickFirstImage();
        homePage.clickSecondImage();
        homePage.clickThirdImage();
        homePage.clickFourthImage();
        homePage.clickFifthImage();

        //прокручую картинки, але ще не придумала як буду перевіряти, типу який експектед ТОчніше це буде відповідна картинка але ще не придумала як це зробити.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void facebookLink(){
        try {
            homePage.goToFacebook();
            socialNetworksPage.verifyFacebookIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void instagramLink(){
        try {
            homePage.goToInstagram();
            socialNetworksPage.verifyInstagramIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void youtubeLink(){
        try {
            homePage.goToYoutube();
            socialNetworksPage.verifyYoutubeIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void twitterLink(){
        try {
            homePage.goToTwitter();
            socialNetworksPage.verifyTwitterIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void linkedinLink(){
        try {
            homePage.goToLinkedin();
            socialNetworksPage.verifyLinkedinIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void googleLink(){
        try {
            homePage.goToGoogle();
            socialNetworksPage.verifyGoogleIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void goldenlineLink(){
        try {
            homePage.goToGoldenline();
            socialNetworksPage.verifyGoldenlineIsOpened();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void verifyAboutUsPage(){
        try {
            homePage.clickAboutUs();
            String aboutUsTitle = aboutUsPage.getPageTitle();
            Assert.assertEquals("ABOUT US",aboutUsTitle);
            aboutUsPage.backToHomePage();
            homePage.verifyHomePageIs();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @After
    public  void tearDown() {
        driver.quit();
    }


}



