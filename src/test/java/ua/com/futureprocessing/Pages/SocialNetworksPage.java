package ua.com.futureprocessing.Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SocialNetworksPage {
    public SocialNetworksPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public WebDriver driver;

    @FindBy (css = "i.fb_logo.img.sp_3OxEQobvphM_1_5x.sx_fd7b0a")
    WebElement logoFacebook;

    @FindBy (css = "._giku3._8scx2.coreSpriteDesktopNavLogoAndWordmark._rujh3")
    WebElement logoInstagram;

    @FindBy (css = ".yt-icon-container.style-scope.ytd-topbar-logo-renderer")
    WebElement logoYoutube;

    @FindBy (css = ".Icon.Icon--bird.Icon--large")
    WebElement logoTwitter;

    @FindBy (css = ".lazy-loaded")
    WebElement logoLinkedin;

    @FindBy (css = ".xdjHt.FYQzvb")
    WebElement logoGoogle;

    @FindBy (css = ".gl-logo")
    WebElement logoGoldenline;

    public void verifyFacebookIsOpened(){
        boolean isPresent = logoFacebook.isDisplayed();
        if (isPresent){
            System.out.println("Facebook is opened");
        }
        else {
            System.out.println("Facebook is not opened");
        }
    }

    public void verifyInstagramIsOpened(){
        boolean isPresent = logoInstagram.isDisplayed();
        if (isPresent){
            System.out.println("Instagram is opened");
        }
        else {
            System.out.println("Instagram is not opened");
        }
    }

    public void verifyYoutubeIsOpened(){
        boolean isPresent = logoYoutube.isDisplayed();
        if (isPresent){
            System.out.println("Youtube is opened");
        }
        else {
            System.out.println("Youtube is not opened");
        }
    }

    public void verifyTwitterIsOpened(){
        boolean isPresent = logoTwitter.isDisplayed();
        if (isPresent){
            System.out.println("Twitter is opened");
        }
        else {
            System.out.println("Twitter is not opened");
        }
    }

    public void verifyLinkedinIsOpened(){
        boolean isPresent = logoLinkedin.isDisplayed();
        if (isPresent){
            System.out.println("Linkedin is opened");
        }
        else {
            System.out.println("Linkedin is not opened");
        }
    }

    public void verifyGoogleIsOpened(){
        boolean isPresent = logoGoogle.isDisplayed();
        if (isPresent){
            System.out.println("Google is opened");
        }
        else {
            System.out.println("Google is not opened");
        }
    }

    public void verifyGoldenlineIsOpened(){
        boolean isPresent = logoGoldenline.isDisplayed();
        if (isPresent){
            System.out.println("Goldenline is opened");
        }
        else {
            System.out.println("Goldenline is not opened");
        }
    }




}
