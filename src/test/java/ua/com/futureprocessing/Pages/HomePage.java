package ua.com.futureprocessing.Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {
    public HomePage (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public WebDriver driver;

    @FindBy(id = "menu-5912")
    WebElement aboutUs;

    @FindBy(id = "submenu-8")
    WebElement header;

    @FindBy(id = "menu-5913")
    WebElement services;

    @FindBy(id = "submenu-66")
    WebElement header1;

    @FindBy(id = "menu-5914")
    WebElement solutions;

    @FindBy(id = "submenu-5911")
    WebElement header2;

    @FindBy(id = "menu-5915")
    WebElement expertise;

    @FindBy(id = "submenu-101")
    WebElement header3;

    @FindBy(id = "menu-5916")
    WebElement clients;

    @FindBy(id = "submenu-client-5290")
    WebElement client;

    @FindBy (id = "menu-5917")
    WebElement contact;

    @FindBy(id = "slick-slide00")
    WebElement firstImage;

    @FindBy(id = "slick-slide01")
    WebElement secondImage;

    @FindBy(id = "slick-slide02")
    WebElement thirdImage;

    @FindBy(id = "slick-slide03")
    WebElement fourthImage;

    @FindBy(id = "slick-slide04")
    WebElement fifthImage;

    @FindBy(css = ".content__socialmedia li:first-child")
    WebElement facebookLink;

    @FindBy(css = ".content__socialmedia li:nth-child(2)")
    WebElement instagramLink;

    @FindBy(css = ".content__socialmedia li:nth-child(3)")
    WebElement youtubeLink;

    @FindBy(css = ".content__socialmedia li:nth-child(4)")
    WebElement twitterLink;

    @FindBy(css = ".content__socialmedia li:nth-child(5)")
    WebElement linkedinLink;

    @FindBy(css = ".content__socialmedia li:nth-child(6)")
    WebElement googleLink;

    @FindBy(css = ".content__socialmedia li:nth-child(7)")
    WebElement goldenlineLink;

    @FindBy (css = ".news-posts__title")
    WebElement news;


    public void mouseOverAboutUs(){
        Actions builder = new Actions(driver);
        builder.moveToElement(aboutUs).build().perform();
    }

    public String getHeaderAboutUs(){
        String headerAboutUs = header.getText();
        return headerAboutUs;
    }

    public void mouseOverServices(){
        Actions builder = new Actions(driver);
        builder.moveToElement(services).build().perform();
    }

    public void mouseOverClients (){
        Actions builder = new Actions(driver);
        builder.moveToElement(clients).build().perform();
    }

    public String getHeaderServices(){
        String servicesHeader = header1.getText();
        return servicesHeader;
    }

    public void mouseOverSolutions(){
        Actions builder = new Actions(driver);
        builder.moveToElement(solutions).build().perform();
    }

    public String getHeaderSolutions(){

        String solutionsHeader = header2.getText();
        return solutionsHeader;
    }

    public void mouseOverExpertise(){
        Actions builder = new Actions(driver);
        builder.moveToElement(expertise).build().perform();
    }

    public String getHeaderExpertise(){
        String expertiseHeader = header3.getText();
        return expertiseHeader;
    }

    public void clickFirstImage(){
        firstImage.click();
    }

    public void clickSecondImage(){
        secondImage.click();
    }

    public void clickThirdImage(){
        thirdImage.click();
    }

    public void clickFourthImage(){
        fourthImage.click();
    }


    public void clickFifthImage(){
        fifthImage.click();
    }

    public void goToFacebook(){
        facebookLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void goToInstagram(){
        instagramLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void goToYoutube(){
        youtubeLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void goToTwitter(){
        twitterLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void goToLinkedin(){
        linkedinLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void goToGoogle(){
        googleLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}

    }

    public void goToGoldenline(){
        goldenlineLink.click();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);}
    }

    public void clickAboutUs(){
        aboutUs.click();
    }

    public void clickServices (){
        services.click();
    }

    public void clickSolutions(){
        solutions.click();
    }

    public void clickExpertise(){
        expertise.click();
    }

    public void clickClients(){
        clients.click();
    }

    public void clickContact(){
        contact.click();
    }

    public void verifyHomePageIs(){
        boolean isPresent = news.isDisplayed();
        if (isPresent){
            System.out.println("HomePage is opened");
        }
        else {
            System.out.println("HomePage is not opened");
        }
    }










}
