package ua.com.futureprocessing.Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AboutUsPage {
    public AboutUsPage (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public WebDriver driver;

    @FindBy (css = ".small-12.medium-6.columns>h1")
    WebElement pageTitle;

    @FindBy (css = ".button.arrow-left")
    WebElement backHome;

    public String getPageTitle(){
        String aboutUs = pageTitle.getText();
        return aboutUs;
    }

    public void backToHomePage(){
        backHome.click();
    }

}
